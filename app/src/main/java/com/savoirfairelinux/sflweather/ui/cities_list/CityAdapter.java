package com.savoirfairelinux.sflweather.ui.cities_list;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.savoirfairelinux.sflweather.R;
import com.savoirfairelinux.sflweather.data.file.model.City;
import com.savoirfairelinux.sflweather.ui.base.BaseViewHolder;
import com.savoirfairelinux.sflweather.ui.city_details.CityDetailsActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class CityAdapter extends RecyclerView.Adapter<CityAdapter.ViewHolder> {

    public static final String KEY_STRING_CITY_ID = "KEY_STRING_CITY_ID";

    private List<City> displayedCities;
    private List<City> allCities;

    private Context context;
    private String filter;

    CityAdapter(@Nullable List<City> allCities) {
        if (allCities == null) {
            allCities = new ArrayList<>();
        }
        this.allCities = allCities;
        displayedCities = new ArrayList<>();
        displayedCities.addAll(allCities);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        View view = LayoutInflater.from(context).inflate(R.layout.layout_city, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final City city = displayedCities.get(position);
        holder.tvCityName.setText(city.getName());
        holder.llCity.setOnClickListener(getOnClickCityListener(city.getId()));
    }

    @Override
    public int getItemCount() {
        return displayedCities.size();
    }

    void addCity(City city) {
        if (city == null) {
            throw new IllegalArgumentException();
        }
        if (filter == null || city.getName().contains(filter)) {
            displayedCities.add(city);
        }
    }

    void filter(String filter) {
        if (filter == null) {
            throw new IllegalArgumentException();
        }

        this.filter = filter;

        Observable.fromIterable(allCities)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .filter(currentCity -> currentCity.getName().toLowerCase().contains(filter.toLowerCase()))
                .toSortedList()
                .subscribe(this::setCities);
    }

    List<City> getDisplayedCities() {
        return displayedCities;
    }

    private void setCities(List<City> cities) {
        displayedCities = cities;
        notifyDataSetChanged();
    }

    private View.OnClickListener getOnClickCityListener(String cityId) {
        return v -> {
            Intent intent = new Intent(context, CityDetailsActivity.class);
            intent.putExtra(KEY_STRING_CITY_ID, cityId);
            context.startActivity(intent);
        };
    }

    static class ViewHolder extends BaseViewHolder {

        @BindView(R.id.llCity)
        View llCity;
        @BindView(R.id.tvCityName)
        TextView tvCityName;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        protected void clear() {
            tvCityName.setText("");
        }
    }
}
