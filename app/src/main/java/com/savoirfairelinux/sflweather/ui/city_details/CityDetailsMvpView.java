package com.savoirfairelinux.sflweather.ui.city_details;

import com.savoirfairelinux.sflweather.data.network.model.CityInfo;
import com.savoirfairelinux.sflweather.ui.base.MvpView;

public interface CityDetailsMvpView extends MvpView {

    void updateWeather(CityInfo cityInfo);

    void onApiError();
}
