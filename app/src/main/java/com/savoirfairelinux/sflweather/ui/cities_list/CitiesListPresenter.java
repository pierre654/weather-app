package com.savoirfairelinux.sflweather.ui.cities_list;

import android.content.Context;

import com.savoirfairelinux.sflweather.data.DataManager;
import com.savoirfairelinux.sflweather.ui.base.BasePresenter;
import com.savoirfairelinux.sflweather.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class CitiesListPresenter<V extends CitiesListMvpView> extends BasePresenter<V>
        implements CitiesListMvpPresenter<V> {

    @Inject
    CitiesListPresenter(DataManager dataManager,
                        SchedulerProvider schedulerProvider,
                        CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void onLoadCities(Context context) {
        if (getDataManager().isFileCompletelyLoaded()) {
            getMvpView().setCityList(getDataManager().getAllCities());
            getMvpView().hideLoading();
            return;
        }

        // Avoid unstable states
        getMvpView().setCityList(getDataManager().getAllCities());

        getMvpView().setMaxProgressBar(getDataManager().initializeFile(context));
        getMvpView().updateProgress(getDataManager().getFileLoadingProgress());
        getMvpView().showLoading();

        getCompositeDisposable().add(
                getDataManager().getCitiesFromFile(context)
                        .subscribeOn(getSchedulerProvider().io())
                        .observeOn(getSchedulerProvider().ui())
                        .skip(1) // Headers line
                        .map(city -> {
                            getDataManager().addCity(city);
                            if (getMvpView() != null) {
                                getMvpView().updateProgress(getDataManager().getFileLoadingProgress());
                                getMvpView().onNext(city);
                            }
                            return city;
                        })
                        .toSortedList()
                        .subscribe(
                                cities -> {
                                    getDataManager().setAllCities(cities);
                                    getMvpView().setCityList(cities);
                                    getMvpView().hideLoading();
                                },
                                this::handleError
                        ));
    }

    @Override
    public void handleError(Throwable throwable) {
        super.handleError(throwable);
        getMvpView().onFileReadError();
    }
}
