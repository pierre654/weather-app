package com.savoirfairelinux.sflweather.ui.city_details;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.savoirfairelinux.sflweather.R;
import com.savoirfairelinux.sflweather.data.network.model.CityInfo;
import com.savoirfairelinux.sflweather.ui.base.BaseFragment;

import java.util.Locale;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A placeholder fragment containing a simple view.
 */
public class CityDetailsFragment extends BaseFragment implements CityDetailsMvpView {

    public static final String TAG = CityDetailsFragment.class.getSimpleName();
    public static final String FRG_KEY_STRING_CITY_ID = "FRG_KEY_STRING_CITY_ID";

    @Inject
    CityDetailsMvpPresenter<CityDetailsMvpView> cityDetailsMvpPresenter;

    @BindView(R.id.tvCityName)
    TextView tvCityName;
    @BindView(R.id.tvDescription)
    TextView tvDescription;
    @BindView(R.id.tvWind)
    TextView tvWind;
    @BindView(R.id.tvPressure)
    TextView tvPressure;
    @BindView(R.id.tvTemp)
    TextView tvTemp;
    @BindView(R.id.tvHumidity)
    TextView tvHumidity;
    @BindView(R.id.pbCityDetails)
    ProgressBar pbCityDetails;

    private String cityId;


    public static CityDetailsFragment newInstance(String cityId) {
        if (cityId == null) {
            throw new IllegalArgumentException("CityDetailsFragment requires argument FRG_KEY_STRING_CITY_ID");
        }

        Bundle bundle = new Bundle();
        bundle.putString(FRG_KEY_STRING_CITY_ID, cityId);

        CityDetailsFragment fragment = new CityDetailsFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_city_details, container, false);

        getAppComponent().inject(this);

        setUnBinder(ButterKnife.bind(this, view));

        cityDetailsMvpPresenter.onAttach(this);

        readArguments();

        return view;
    }

    private void readArguments() {
        Bundle arguments = getArguments();
        if (arguments == null) {
            throw new IllegalArgumentException("CityDetailsFragment requires argument FRG_KEY_STRING_CITY_ID");
        }

        cityId = arguments.getString(FRG_KEY_STRING_CITY_ID);
        if (cityId == null) {
            throw new IllegalArgumentException("No cityId for CityDetailsFragment");
        }
    }

    @Override
    public void showLoading() {
        pbCityDetails.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        pbCityDetails.setVisibility(View.GONE);
    }

    @Override
    protected void setUp(View view) {

        cityDetailsMvpPresenter.onLoadCity(cityId);
    }

    @Override
    public void updateWeather(CityInfo cityInfo) {
        tvCityName.setText(cityInfo.getName());
        tvDescription.setText(cityInfo.getWeather().get(0).getDescription());
        if (cityInfo.getWind().getDeg() == null) {
            tvWind.setText(String.format(Locale.getDefault(), "%.0f m/sec", cityInfo.getWind().getSpeed()));
        } else {
            tvWind.setText(String.format(Locale.getDefault(), "%.0f° | %.0f m/sec", cityInfo.getWind().getDeg(), cityInfo.getWind().getSpeed()));
        }
        tvPressure.setText(String.format(Locale.getDefault(), "%.0f hPa ", cityInfo.getMain().getPressure()));
        tvTemp.setText(String.format(Locale.getDefault(), "%.0f°C", cityInfo.getMain().getTemp()));
        tvHumidity.setText(String.format(Locale.getDefault(), "%d%%", cityInfo.getMain().getHumidity()));
    }

    @Override
    public void onApiError() {
        showMessage(R.string.error_message_api_failure);

        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    @Override
    public void onDestroyView() {
        cityDetailsMvpPresenter.onDetach();
        super.onDestroyView();
    }
}
