package com.savoirfairelinux.sflweather.ui.city_details;

import com.savoirfairelinux.sflweather.data.DataManager;
import com.savoirfairelinux.sflweather.ui.base.BasePresenter;
import com.savoirfairelinux.sflweather.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class CityDetailsPresenter<V extends CityDetailsMvpView> extends BasePresenter<V> implements
        CityDetailsMvpPresenter<V> {

    @Inject
    CityDetailsPresenter(DataManager dataManager,
                         SchedulerProvider schedulerProvider,
                         CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void onLoadCity(String cityId) {
        if (cityId == null) {
            throw new IllegalArgumentException();
        }

        getMvpView().showLoading();

        getCompositeDisposable().add(
                getDataManager().getWeatherByCityIdApiCall(cityId)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                cityInfo -> {
                                    if (getMvpView() != null) {
                                        getMvpView().updateWeather(cityInfo);
                                        getMvpView().hideLoading();
                                    }
                                },
                                this::handleError
                        ));
    }

    @Override
    public void handleError(Throwable throwable) {
        super.handleError(throwable);
        getMvpView().hideLoading();
        getMvpView().onApiError();
    }
}
