package com.savoirfairelinux.sflweather.ui.city_details

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.NavUtils
import android.view.MenuItem
import com.savoirfairelinux.sflweather.R
import com.savoirfairelinux.sflweather.ui.base.BaseActivity
import com.savoirfairelinux.sflweather.ui.cities_list.CityAdapter
import kotlinx.android.synthetic.main.activity_city_details.*

class CityDetailsActivity : BaseActivity() {

    private var cityId: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_city_details)

        readArguments()

        setUp()
    }

    fun readArguments() {
        val extras = intent.extras ?: throw IllegalArgumentException("onCreate(): this activity requires KEY_STRING_CITY_ID extra")
        cityId = extras.getString(CityAdapter.KEY_STRING_CITY_ID) ?: throw IllegalArgumentException("No cityId for CityDetailsActivity")
    }

    override fun setUp() {
        setSupportActionBar(toolbar)

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        supportFragmentManager
                .beginTransaction()
                .disallowAddToBackStack()
                .add(R.id.flFragmentContainer, CityDetailsFragment.newInstance(cityId), CityDetailsFragment.TAG)
                .commit()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                val upIntent = NavUtils.getParentActivityIntent(this)
                upIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                NavUtils.navigateUpTo(this, upIntent)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
