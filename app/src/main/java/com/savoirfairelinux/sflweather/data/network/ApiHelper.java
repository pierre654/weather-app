package com.savoirfairelinux.sflweather.data.network;

import com.savoirfairelinux.sflweather.data.network.model.CityInfo;

import io.reactivex.Observable;

public interface ApiHelper {

    Observable<CityInfo> getWeatherByCityIdApiCall(String cityId);
}
