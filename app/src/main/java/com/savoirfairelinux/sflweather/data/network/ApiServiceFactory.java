package com.savoirfairelinux.sflweather.data.network;

import com.savoirfairelinux.sflweather.BuildConfig;
import com.savoirfairelinux.sflweather.MvpApp;
import com.savoirfairelinux.sflweather.data.network.model.Units;
import com.savoirfairelinux.sflweather.utils.NetworkUtils;

import java.util.Locale;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.CacheControl;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import timber.log.Timber;

class ApiServiceFactory {

    private static final String BASE_URL_OPEN_WEATHER_MAP = "https://api.openweathermap.org";

    private static OpenWeatherMapService openWeatherMapService;

    static OpenWeatherMapService getOpenWeatherMapService() {
        if (openWeatherMapService == null) {
            openWeatherMapService = getRetrofit(BASE_URL_OPEN_WEATHER_MAP)
                    .create(OpenWeatherMapService.class);
        }
        return openWeatherMapService;
    }

    private static Retrofit getRetrofit(String baseUrl) {
        if (baseUrl == null || baseUrl.trim().isEmpty() || !NetworkUtils.isUrl(baseUrl)) {
            throw new IllegalArgumentException("Invalid baseUrl");
        }

        OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .addInterceptor(getAuthenticationInterceptor())
                .addInterceptor(provideOfflineCacheInterceptor())
                .addNetworkInterceptor(provideCacheInterceptor())
                .cache(getCache());

        if (BuildConfig.DEBUG) {
            Timber.w("getRetrofit(): Retrofit Log activated");
            builder.addInterceptor(getLoggingInterceptor());
        }

        return new Retrofit.Builder()
                .client(builder.build())
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }

    private static Interceptor getAuthenticationInterceptor() {
        return chain -> {
            Request original = chain.request();
            HttpUrl originalHttpUrl = original.url();

            HttpUrl url = originalHttpUrl.newBuilder()
                    .addQueryParameter("appid", BuildConfig.OPEN_WEATHER_MAP_API_TOKEN)
                    .addQueryParameter("units", Units.METRIC.name)
                    .addQueryParameter("lang", Locale.getDefault().toString())
                    .build();

            // Request customization: add request headers
            Request.Builder requestBuilder = original.newBuilder()
                    .url(url);

            Request request = requestBuilder.build();
            return chain.proceed(request);
        };
    }

    private static Interceptor getLoggingInterceptor() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        return logging;
    }


    private static Interceptor provideCacheInterceptor() {
        return chain -> {
            Response response = chain.proceed(chain.request());

            // Recommended 10 min cache. See http://openweathermap.org/appid
            CacheControl cacheControl = new CacheControl.Builder()
                    .maxAge(10, TimeUnit.MINUTES)
                    .build();

            return response.newBuilder()
                    .header("Cache-Control", cacheControl.toString())
                    .build();
        };
    }

    private static Interceptor provideOfflineCacheInterceptor() {
        return chain -> {
            Request request = chain.request();

            CacheControl cacheControl = new CacheControl.Builder()
                    .maxStale(7, TimeUnit.DAYS)
                    .build();

            request = request.newBuilder()
                    .cacheControl(cacheControl)
                    .build();

            return chain.proceed(request);
        };
    }

    private static Cache getCache() {
        int cacheSizeBytes = 10 * 1024 * 1024;
        return new Cache(MvpApp.getInstance().getCacheDir(), cacheSizeBytes);
    }
}