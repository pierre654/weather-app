package com.savoirfairelinux.sflweather.data.file;

import android.content.Context;

import com.savoirfairelinux.sflweather.data.file.model.City;

import java.util.List;

import io.reactivex.Observable;

public interface FileHelper {

    Observable<City> getCitiesFromFile(Context context);

    List<City> getAllCities();

    void setAllCities(List<City> cities);

    boolean isFileCompletelyLoaded();

    int initializeFile(Context context);

    int getFileLoadingProgress();

    void addCity(City city);
}
