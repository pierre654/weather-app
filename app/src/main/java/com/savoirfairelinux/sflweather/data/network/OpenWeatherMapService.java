package com.savoirfairelinux.sflweather.data.network;

import com.savoirfairelinux.sflweather.data.network.model.CityInfo;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

interface OpenWeatherMapService {

    @GET("/data/2.5/weather")
    Observable<CityInfo> getWeatherByCityIdApiCall(@Query("id") String cityId);
}
