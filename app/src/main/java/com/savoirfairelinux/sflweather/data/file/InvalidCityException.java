package com.savoirfairelinux.sflweather.data.file;

class InvalidCityException extends Throwable {
    InvalidCityException(String message) {
        super(message);
    }
}