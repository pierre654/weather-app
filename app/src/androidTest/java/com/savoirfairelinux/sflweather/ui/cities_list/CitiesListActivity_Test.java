package com.savoirfairelinux.sflweather.ui.cities_list;

import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.savoirfairelinux.sflweather.R;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static com.savoirfairelinux.sflweather.test_utils.ToolbarUtils.matchToolbarTitle;

@RunWith(AndroidJUnit4.class)
public class CitiesListActivity_Test {

    @Rule
    public ActivityTestRule<CitiesListActivity> CitiesListActivityActivityTestRule = new ActivityTestRule<>(CitiesListActivity.class);

    @Test
    public void CitiesListActivityTest() {

        // Click on first city in list
        onView(withId(R.id.rvCities))
                .perform(RecyclerViewActions.actionOnItemAtPosition(0, click()));

        // Check that we have been redirected to city details
        matchToolbarTitle(InstrumentationRegistry.getTargetContext().getString(R.string.title_activity_city_details));
    }
}
