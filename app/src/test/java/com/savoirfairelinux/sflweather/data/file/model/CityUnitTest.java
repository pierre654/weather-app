package com.savoirfairelinux.sflweather.data.file.model;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * City local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class CityUnitTest {

    private static final String CITY_ID = "123";
    private static final String CITY_NAME = "Montréal";

    @Test
    public void init_test() {
        City city = new City(CITY_ID, CITY_NAME);

        assertEquals(CITY_ID, city.getId());
        assertEquals(CITY_NAME, city.getName());
    }

    @Test
    public void compareTo_test() {
        City city = new City(CITY_ID, CITY_NAME);
        City city2 = new City(CITY_ID, CITY_NAME);

        assertEquals(0, city.compareTo(city2));
        assertEquals(city.getId(), city2.getId());
        assertEquals(city.getName(), city2.getName());
    }

    @Test
    public void compareTo_case_test() {
        City city = new City(CITY_ID, "a");
        City city2 = new City(CITY_ID, "A");

        assertEquals(0, city.compareTo(city2));
    }

    @Test
    public void compareTo_less_test() {
        City city = new City(CITY_ID, CITY_NAME);
        City city2 = new City(CITY_ID, "a");

        assertTrue(city.compareTo(city2) > 0);
    }

    @Test
    public void compareTo_more_test() {
        City city = new City(CITY_ID, CITY_NAME);
        City city2 = new City(CITY_ID, "z");

        assertTrue(city.compareTo(city2) < 0);
    }

    @Test
    public void equals_test() {
        City city = new City(CITY_ID, "a");
        City city2 = new City(CITY_ID, "A");

        assertTrue(city.equals(city2));
    }

    @Test
    public void not_equals_test() {
        City city = new City(CITY_ID, CITY_NAME);
        City city2 = new City("456", CITY_NAME);

        assertFalse(city.equals(city2));
    }

    @Test
    public void equals_null_test() {
        City city = new City(CITY_ID, CITY_NAME);

        //noinspection ObjectEqualsNull
        assertFalse(city.equals(null));
    }

    @Test
    public void toString_test() {
        City city = new City(CITY_ID, CITY_NAME);

        assertEquals("City(id=" + city.getId() + ", name=" + city.getName() + ")", city.toString());
    }
}