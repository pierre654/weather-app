package com.savoirfairelinux.sflweather.ui.cities_list;

import com.savoirfairelinux.sflweather.data.file.model.City;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class CityAdapterTest {

    private static final List<City> CANADIAN_CITIES = Arrays.asList(
            new City("1", "Toronto"),
            new City("2", "Montréal"),
            new City("3", "Calgary"),
            new City("4", "Ottawa"),
            new City("5", "Edmonton")
    );

    @Test
    public void init_test() throws Exception {
        CityAdapter cityAdapter = new CityAdapter(CANADIAN_CITIES);

        List<City> displayedCities = cityAdapter.getDisplayedCities();
        int size = displayedCities.size();

        assertEquals(CANADIAN_CITIES.size(), size);
        assertEquals(CANADIAN_CITIES.get(0), displayedCities.get(0));
        assertEquals(CANADIAN_CITIES.get(1), displayedCities.get(1));
        assertEquals(CANADIAN_CITIES.get(2), displayedCities.get(2));
        assertEquals(CANADIAN_CITIES.get(3), displayedCities.get(3));
        assertEquals(CANADIAN_CITIES.get(4), displayedCities.get(4));
    }

    @Test
    public void init_null_test() throws Exception {
        CityAdapter cityAdapter = new CityAdapter(null);

        assertTrue(cityAdapter.getDisplayedCities().isEmpty());
    }

    @Test
    public void addCity_test() throws Exception {
        CityAdapter cityAdapter = new CityAdapter(CANADIAN_CITIES);

        int oldItemCount = cityAdapter.getItemCount();
        cityAdapter.addCity(new City("6", "Mississauga"));

        assertEquals(oldItemCount + 1, cityAdapter.getItemCount());
    }

    @Test(expected = IllegalArgumentException.class)
    public void addCity_null_test() throws Exception {
        CityAdapter cityAdapter = new CityAdapter(CANADIAN_CITIES);

        cityAdapter.addCity(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void filter_null_test() throws Exception {
        CityAdapter cityAdapter = new CityAdapter(CANADIAN_CITIES);

        cityAdapter.filter(null);
    }
}