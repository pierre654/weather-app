

OBJECTIF
--------

Construire une app météo en 2 écrans:

* Écran 1: Afficher la liste des villes chargées depuis le fichier texte city_list.txt
Le fichier est déjà packagé et lu dans l'app.
La liste doit simplement afficher le nom de la ville
Quand on clique sur une ville on passe à l'écran 2.

Optionnel: Faire en sorte que le champ Search filtre la liste des villes.

* Écran 2: Afficher les détails météo pour la ville choisie
Il y a déja une UI basique (à améliorer optionnelement)
Il faut réaliser un appel à l'API d'openweathermap (voir section INFO)

REGLES
-----

1. La UI ne doit pas freezer
2. Choisir un motif d'architecture efficace
3. Corriger les éventuelles erreurs présentes dans le projet
4. L'utilisation de git (avec des commits en local) est aussi évaluée

INFO
----

API Token: 996d49bf70e75c98a48cfddf2ea7ffa7

Description API
https://openweathermap.org/current

Liste de villes
http://openweathermap.org/help/city_list.txt


